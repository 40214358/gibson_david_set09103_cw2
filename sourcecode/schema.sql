DROP TABLE if EXISTS user;
DROP TABLE if EXISTS joke;

CREATE TABLE user (
 username TEXT NOT NULL PRIMARY KEY UNIQUE,
 email TEXT NOT NULL UNIQUE,
 user_password TEXT NOT NULL UNIQUE,
 date_of_birth TEXT NOT NULL, 
 is_administrator INT NOT NULL, 
 subscribers TEXT
);

CREATE TABLE joke (
 joke_id INTEGER PRIMARY KEY,
 content TEXT NOT NULL,
 username TEXT NOT NULL,
 date_posted TEXT NOT NULL, 
 likes TEXT NOT NULL
);


INSERT INTO user (
    username, 
    email,
    user_password,
    date_of_birth,
    is_administrator, 
    subscribers
) VALUES (
    'TheJoker',
    'stevenj129@btinternet.com',
    'killthebatman',
    '10/02/1989',
    '0',
    ', ComedyKing, SupermansCape'
),
(
    'HomerSimpson',
    'HSimpson@hotmail.com',
    'thesimpsons',
    '10/02/1989',
    '0',
    ', TheJoker, ComedyKing, SupermansCape, MrTest, JohnSmith'
),
(
    'MrTest',
    'mrTesting@gmail.com',
    'Snickers',
    '10/05/1950',
    '0',
    ', HomerSimpson, SupermansCape'
),
(
    'ComedyKing',
    'cking@googlemail.com',
    'standupfortheking',
    '19/11/1970',
    '1', 
    ', TheJoker, JohnSmith'
),
(
    'JohnSmith',
    'john@btinternet.com',
    'boringpassword',
    '19/11/1970',
    '1', 
    ', TheJoker'
),
(
    'SupermansCape',
    'Calel@krypto.co.uk',
    'originalhero',
    '19/11/1998',
    '0', 
    ', TheJoker, ComedyKing, John Smith'
);

INSERT INTO joke (
    joke_id,
    content, 
    username,
    date_posted,
    likes
) VALUES (
    '00000009',
    'I have a Polish friend who is a sound technician. Oh, and a Czech one too. Czech one too. Czech one too.',
    'TheJoker', 
    '02/05/2018',
    ', ComedyKing, SupermansCape'
),
(
    '00000008',
    'A Ham Sandwich walks into a bar. The bartender says "I''m sorry, We don''t serve food here".',
    'ComedyKing', 
    '23/04/2018',
    ', SupermansCape'
),
(
    '00000007',
    'Yo Mama is so ugly, her portraits hang themselves.',
    'TheJoker', 
    '29/02/2018',
    ', SupermansCape, MrTest'
),
(
    '00000006',
    'An Englishman, an Irishman, a Scotsman, a German, a Lithuanian, an American, a Vietnamese, a Japanese, a Korean, an Angolan, a South African, a Greenlander, a Russian, a Saudi Arabian, an Iraqi, a Pakistani, a Cambodian, a Norwegian, an Albanian, an Argentinian, a Jamaican, an Ethiopian, a Kenyan, a Kuwaiti, a Romanian, a Zambian, a Venezuelan, a Tunisian, and a Peruvian all go to a very classy nightclub. The bouncer stops them and says, "I''m sorry, you can''t come in without a Thai."',
    'SupermansCape', 
    '11/02/2018',
    ', ComedyKing'
),
(
    '00000005',
    'How did the dentist become a brain surgeon? His hand slipped.',
    'MrTest', 
    '10/04/2017',
    ', SupermansCape, TheJoker, MrTest'
),
(
    '00000004',
    'I was going to tell a time travelling joke, but you guys didn''t like it',
    'MrTest', 
    '22/03/2017',
    ', HomerSimpson'
),
(
    '00000003',
    'My 3 favourite things in life are eating my family and not using commas',
    'ComedyKing', 
    '23/03/2017',
    ', SupermansCape, ComedyKing, MrTest'
),
(
    '00000002',
    'At any given moment the urge to sing, "The Lion Sleeps Tonight" is just a whim away, a whim away, a whim away.',
    'TheJoker', 
    '13/01/2017',
    ', SupermansCape, ComedyKing, MrTest'
),
(
    '00000001',
    'People are often shocked when they find out that I''m not a very good electrician',
    'HomerSimpson', 
    '12/12/2016',
    ', SupermansCape, ComedyKing, MrTest'
);