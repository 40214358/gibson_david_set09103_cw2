from flask import Flask, request, render_template, redirect, url_for, session, g
from functools import wraps
from datetime import datetime
import sqlite3
import bcrypt 
import sys

app = Flask(__name__)
db_location = 'var/jokes.db'

app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'

def get_db():
    db = getattr(g, 'db', None) 
    if db is None:
        db = sqlite3.connect(db_location)
        g.db = db 
    return db

@app.teardown_appcontext
def close_db_connection(exception): 
    db = getattr(g, 'db', None)
    if db is not None:
        db.close()

def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource('schema.sql', mode='r') as f:
            db.cursor().executescript(f.read())
        db.commit()

def feed():
    db = get_db()
    cursor = db.cursor()
    cursor.execute('SELECT * FROM joke ORDER BY joke_id DESC')
    rows = cursor.fetchall()
    return rows

@app.route('/favourites', methods=['GET'])
def get_liked_jokes():
    currentUser = session.get('currentUser')
    db = get_db()
    cursor = db.cursor()  
    cursor.execute('SELECT * FROM joke WHERE likes LIKE ? ORDER BY joke_id DESC', ['%'+currentUser[0]+'%'])
    rows = cursor.fetchall()
    return render_template('favourites.html', jokes=rows, user=currentUser), 200

@app.route('/subscriptions', methods=['GET', 'POST'])
def get_subscribed_users_jokes():
    currentUser = session.get('currentUser')
    db = get_db()
    cursor = db.cursor()  
    cursor.execute('SELECT joke.joke_id, joke.content, joke.username, joke.date_posted, joke.likes FROM joke INNER JOIN user ON joke.username=user.username WHERE user.subscribers LIKE ? ORDER BY joke_id DESC', ['%'+currentUser[0]+'%'])
    rows = cursor.fetchall()
    if request.method == 'POST' and 'image' and 'button' in request.form:
        id = format(request.form['joke-id'])
        joke_id = id
        joke = get_joke(joke_id)
        updated_likes = joke[4] + ", " + currentUser[0]
        db = get_db()
        cur = db.cursor()
        cur.execute("UPDATE joke SET likes = ? WHERE joke_id=?", (updated_likes, joke_id) )
        db.commit()
        return redirect(url_for('get_subscribed_users_jokes'))
    return render_template('subscriptions.html', jokes=rows, user=currentUser), 200

def get_users_jokes(username):
    db = get_db()
    cursor = db.cursor()  
    cursor.execute('SELECT * FROM joke WHERE username = ? ORDER BY joke_id DESC', [username])
    rows = cursor.fetchall()
    return rows

def get_users():
    db = get_db()
    cursor = db.cursor()
    cursor.execute('SELECT * FROM user ORDER BY username')
    rows = cursor.fetchall()
    return rows

def get_leaderboard():
    db = get_db()
    cursor = db.cursor()
    cursor.execute('SELECT * FROM user ORDER BY LENGTH(subscribers) DESC')
    rows = cursor.fetchall()
    return rows

def get_user(username):
    user = query_db('SELECT * FROM user WHERE username = ?',
    [username], one=True)
    return user

def get_user_string_input(username):
    db = get_db()
    cursor = db.cursor()  
    cursor.execute('SELECT * FROM joke WHERE username = ? ORDER BY joke_id DESC', (username)) 
    user = cursor.fetchall()
    return user

def get_joke_id():
    latest_id = query_db('SELECT MAX(joke_id) FROM joke', one=True)
    new_id = latest_id[0]+1
    return new_id

def get_joke(id):
    joke = query_db('SELECT * FROM joke WHERE joke_id = ?',
    [id], one=True)
    return joke

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

def check_auth(email, password):
    currentUser = None
    user= query_db('SELECT * FROM user WHERE email = ?',
    [email], one=True)
    # hashing is working properly, however, it is like this (by design) for the demo
    # If not like this the non hashed passwords in the demo seed files would fail
    valid_pwhash = bcrypt.hashpw(user[2].encode('utf-8'), bcrypt.gensalt())
    if(email == user[1] and
        valid_pwhash == bcrypt.hashpw(password.encode('utf-8'),
        valid_pwhash)):
            currentUser = user
    return currentUser

def requires_login(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        status = session.get('logged_in', False) 
        if not status:
            return redirect(url_for('.login')) 
        return f(*args, **kwargs)
    return decorated

@app.route('/logout') 
def logout():
    session['logged_in'] = False 
    session['currentUser'] = None
    session.clear()
    return redirect(url_for('.login'))

@app.route('/', methods=['GET', 'POST'])
@requires_login
def root():
    rows=feed()
    currentUser = session.get('currentUser')
    if request.method == 'POST' and 'image' and 'button' in request.form:
        id = format(request.form['joke-id'])
        joke_id = id
        joke = get_joke(joke_id)
        updated_likes = joke[4] + ", " + currentUser[0]
        db = get_db()
        cur = db.cursor()
        cur.execute("UPDATE joke SET likes = ? WHERE joke_id=?", (updated_likes, joke_id) )
        db.commit()
        return redirect(url_for('root'))
    if request.method == 'POST' and request.form['joke-id'] is not None and request.form['joke-id'] != "":
        id = format(request.form['joke-id'])
        joke_id = id
        db = get_db()
        cur = db.cursor()
        cur.execute("DELETE FROM joke WHERE joke_id = ?", [joke_id])
        db.commit()
        return redirect(url_for('root'))
    if request.method == 'POST':
        joke_id = get_joke_id()
        content = format(request.form['text'])
        username = currentUser[0]
        date_posted = datetime.now().strftime('%d/%m/%Y')
        likes = ''
        db = get_db()
        cur = db.cursor()
        cur.execute("INSERT INTO joke (joke_id,content,username,date_posted,likes) VALUES (?,?,?,?,?)",(joke_id,content,username,date_posted,likes) )
        db.commit()
        return redirect(url_for('root'))
    return render_template('home.html', jokes=rows, user=currentUser), 200

@app.route("/home")
def home():
    return redirect(url_for('root'))

@app.route('/admin_panel', methods=['GET', 'POST'])
@requires_login
def admin_view_users():
    rows=get_users()
    currentUser = session.get('currentUser')
    if request.method == 'POST' and request.form['username'] is not None:
        id = format(request.form['username'])
        username = id
        db = get_db()
        cur = db.cursor()
        cur.execute("DELETE FROM user WHERE username=?",[username])
        db.commit()
        return redirect(url_for('admin_view_users'))
    return render_template('adminViewUsers.html', users=rows, user=currentUser), 200

@app.route('/discover', methods=['GET', 'POST'])
@requires_login
def view_users():
    rows=get_users()
    currentUser = session.get('currentUser')
    if request.method == 'POST' and request.form['username'] is not None:
        id = format(request.form['username'])
        username = id
        user = get_user(username)
        updated_subscribers = user[5] + currentUser[0] + ','
        db = get_db()
        cur = db.cursor()
        cur.execute("UPDATE user SET subscribers = ? WHERE username = ?",(updated_subscribers, username))
        db.commit()
        return redirect(url_for('view_users'))
    return render_template('viewUsers.html', users=rows, user=currentUser, currentUser=currentUser), 200

@app.route('/leaderboard', methods=['GET'])
@requires_login
def leaderboard():
    rows=get_leaderboard()
    currentUser = session.get('currentUser')
    return render_template('leaderboard.html', users=rows, user=currentUser, currentUser=currentUser), 200

@app.route("/login", methods=['GET', 'POST']) 
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        if check_auth(email, password) is not None :
            currentUser = check_auth(email, password)
            session['currentUser'] = currentUser
            session['logged_in'] = True
            return redirect(url_for('.root')) 
    return render_template('login.html')

@app.route("/register", methods=['GET', 'POST']) 
def register():
    if request.method == 'POST':
        username = request.form['username']
        email = request.form['email']
        user_password = request.form['user_password']
        date_of_birth = request.form['date_of_birth']
        is_administrator = 0
        db = get_db()
        cur = db.cursor()
        cur.execute("INSERT INTO user (username,email,user_password,date_of_birth,is_administrator) VALUES (?,?,?,?,?)",(username,email,user_password,date_of_birth,is_administrator) )
        db.commit()
        return render_template('login.html')
    return render_template('register.html')

@app.route("/profile", methods=['GET', 'POST']) 
def profile():
    currentUser = session.get('currentUser')
    username = currentUser[0]
    jokes = get_users_jokes(username)
    if request.method == 'POST' and 'image' and 'button' in request.form:
        id = format(request.form['joke-id'])
        joke_id = id
        joke = get_joke(joke_id)
        updated_likes = joke[4] + ", " + currentUser[0]
        db = get_db()
        cur = db.cursor()
        cur.execute("UPDATE joke SET likes = ? WHERE joke_id=?", (updated_likes, joke_id) )
        db.commit()
        return redirect(url_for('profile'))
    return render_template('profile.html', user=currentUser, profileUser=currentUser, jokes=jokes)

@app.route("/profile/<key>", methods=['GET', 'POST'])
def other_profile(key):
    currentUser = session.get('currentUser')
    if request.method == 'POST' and 'image' and 'button' in request.form:
        id = format(request.form['joke-id'])
        joke_id = id
        joke = get_joke(joke_id)
        updated_likes = joke[4] + ", " + currentUser[0]
        db = get_db()
        cur = db.cursor()
        cur.execute("UPDATE joke SET likes = ? WHERE joke_id=?", (updated_likes, joke_id) )
        db.commit()
        return redirect(url_for('other_profile', key=key))
    try: 
        user = get_user(key)
        jokes = get_users_jokes(key)
        return render_template('profile.html', user=currentUser, profileUser=user, jokes=jokes)
    except KeyError:
        return render_template('404.html'), 404

@app.errorhandler(404)
def page_not_found(error):
    currentUser = session.get('currentUser')
    return render_template('404.html', user=currentUser), 404

@app.route('/config/') 
def config():
    str = []
    str.append('Debug:'+app.config['DEBUG']) 
    str.append('port:'+app.config['port']) 
    str.append('url:'+app.config['url']) 
    str.append('ip_address:'+app.config['ip_address']) 
    return '\t'.join(str)

def init(app):
    config = ConfigParser.ConfigParser() 
    try:
        config_location = "etc/defaults.cfg" 
        config.read(config_location)
        app.config['DEBUG'] = config.get("config", "debug") 
        app.config['ip_address'] = config.get("config", "ip_address")
        app.config['port'] = config.get("config", "port") 
        app.config['url'] = config.get("config", "url")
    except:
        print("Could not read configs from: ", config_location)


if __name__ == "__main__":
    init(app)
    app.run(
        host=app.config['ip_address'],
        port=int(app.config['port']))